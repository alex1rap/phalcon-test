create table if not exists t_categories
(
  category_id        int auto_increment
    primary key,
  category_title     varchar(32)     not null,
  category_parent_id int default '0' not null
);

create table if not exists t_product_categories
(
  product_category_id bigint auto_increment
    primary key,
  category_id         int    not null,
  product_id          bigint not null
);

create table if not exists t_products
(
  product_id          bigint auto_increment
    primary key,
  product_title       varchar(32)     not null,
  product_description text            null,
  product_made_time   int default '0' not null,
  product_price       int default '0' not null,
  product_amount      int default '0' not null
);

INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (1, 'Electronic', 0);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (2, 'Hobby', 0);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (3, 'Food', 0);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (4, 'Phones', 1);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (5, 'Tablets', 1);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (6, 'Notebooks', 1);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (7, 'Desktops', 1);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (8, 'Bikes', 2);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (9, 'Fish-rods', 2);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (10, 'Water', 3);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (11, 'Pizzas', 3);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (12, 'Sushi', 3);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (13, 'Meizu', 4);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (14, 'Nokia', 4);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (15, 'Xiaomi', 4);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (16, 'Doogee', 4);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (17, 'Lenovo', 4);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (18, 'Lenovo', 5);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (19, 'Gateway', 6);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (20, 'Hewlett Packard', 6);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (21, 'Lenovo', 6);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (22, 'Asus', 6);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (23, 'Acer', 6);
INSERT INTO t_categories (category_id, category_title, category_parent_id) VALUES (24, 'Asus', 4);
INSERT INTO t_product_categories (product_category_id, category_id, product_id) VALUES (1, 13, 1);
INSERT INTO t_product_categories (product_category_id, category_id, product_id) VALUES (2, 15, 2);
INSERT INTO t_product_categories (product_category_id, category_id, product_id) VALUES (3, 13, 3);
INSERT INTO t_products (product_id, product_title, product_description, product_made_time, product_price, product_amount) VALUES (1, 'Meizu M5s', 'Диагональ экрана: 5.2";
Разрешение экрана: 1280x720;
Камера: 13 Мп;
Количество ядер: 8;
Оперативная память: 3 Гб;
Внутренняя память: 32 Гб;', 0, 3199, 0);
INSERT INTO t_products (product_id, product_title, product_description, product_made_time, product_price, product_amount) VALUES (2, 'Xiaomi Redmi 5 Plus', 'Диагональ экрана: 5.99";
Разрешение экрана: 2160x1080;
Камера: 12 Мп;
Количество ядер: 8;
Оперативная память: 4 Гб;
Внутренняя память: 64 Гб;', 0, 5999, 0);
INSERT INTO t_products (product_id, product_title, product_description, product_made_time, product_price, product_amount) VALUES (3, 'Meizu M3 Note', 'Диагональ экрана: 5.5";
Разрешение экрана: 1920x1080;
Камера: 13 Мп;
Количество ядер: 8;
Оперативная память: 3 Гб;
Внутренняя память: 32 Гб;', 0, 4443, 0);