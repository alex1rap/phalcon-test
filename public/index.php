<?php
/**
 * Created by PhpStorm.
 * User: alex1rap
 * Date: 23.10.18
 * Time: 17:23
 */

ini_set('display_errors', true);
error_reporting(E_ALL);
mb_internal_encoding("utf-8");

use Phalcon\Di\FactoryDefault;
use Phalcon\Loader;
use Phalcon\Mvc\Application;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Url as UrlProvider;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;

// Определяем некоторые константы с абсолютными путями
// для использования с локальными ресурасами
define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');
// ...

function makePlaceholder($params)
{
    $keys = [];
    foreach ($params as $key => $value)
    {
        $keys[] = "?{$key}";
    }
    return implode(', ', $keys);
}

function debug($data, $return = false)
{
    ob_start();
    echo "<pre>";
    var_dump($data);
    echo "</pre>";
    $result = ob_get_clean();
    if ($return) return $result;
    echo $result;
    return true;
}

$loader = new Loader();

$loader->registerDirs(
    [
        APP_PATH . '/controllers/',
        APP_PATH . '/models/',
    ]
);

$loader->register();

$di = new FactoryDefault();

$di->set(
    'view',
    function () {
        $view = new View();
        $view->setViewsDir(APP_PATH . '/views/');
        return $view;
    }
);

$di->set(
    'url',
    function () {
        $url = new UrlProvider();
        $url->setBaseUri('/');
        return $url;
    }
);

$di->set(
    'db',
    function () {
        return new DbAdapter([
            'host'     => 'localhost',
            'username' => 'phalcon',
            'password' => 'Phal$con2018',
            'dbname'   => 'phalcon',
            'charset' => 'utf8'
        ]);
    }
);

$application = new Application($di);
$response = $application->handle();
$response->send();
