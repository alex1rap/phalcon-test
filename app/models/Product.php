<?php

/**
 * Created by PhpStorm.
 * User: alex1rap
 * Date: 25.10.18
 * Time: 13:56
 */


use Phalcon\Mvc\Model;

class Product extends Model
{
    public function initialize()
    {
        $this->setSource('t_products');
    }

    public $id;
    public $title;
    public $description;
    public $madeTime;
    public $price;
    public $amount;

    public function columnMap()
    {
        // Keys are the real names in the table and
        // the values their names in the application
        return [
            'product_id'       => 'id',
            'product_title' => 'title',
            'product_description' => 'description',
            'product_made_time' => 'madeTime',
            'product_price' => 'price',
            'product_amount' => 'amount'
        ];
    }
}