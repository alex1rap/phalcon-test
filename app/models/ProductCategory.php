<?php

/**
 * Created by PhpStorm.
 * User: alex1rap
 * Date: 25.10.18
 * Time: 15:16
 */

use Phalcon\Mvc\Model;

class ProductCategory extends Model
{
    public function initialize()
    {
        $this->setSource('t_product_categories');
    }

    public $id;
    public $categoryId;
    public $productId;

    public function columnMap()
    {
        return [
            'product_category_id' => 'id',
            'category_id' => 'categoryId',
            'product_id' => 'productId'
        ];
    }
}