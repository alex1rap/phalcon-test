<?php
/**
 * Created by PhpStorm.
 * User: alex1rap
 * Date: 25.10.18
 * Time: 15:18
 */

use Phalcon\Mvc\Model;

class Category extends Model
{
    public function initialize()
    {
        $this->setSource('t_categories');
    }

    public $id;
    public $title;
    public $parentId;

    public function columnMap()
    {
        return [
            'category_id' => 'id',
            'category_title' => 'title',
            'category_parent_id' => 'parentId'
        ];
    }
}
