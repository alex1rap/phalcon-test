<?php
/**
 * Created by PhpStorm.
 * User: alex1rap
 * Date: 23.10.18
 * Time: 17:46
 */

class IndexController extends \Phalcon\Mvc\Controller
{

    public function smartphonesAction()
    {
        $currentCategory = [];
        $categoryId = $_GET['category'] ?: 0;
        $categories = Category::find([
            'id=?0 OR parentId=?0',
            'bind' => [$categoryId]
        ])->toArray();
        $categoryIds = [0];
        foreach ($categories as $category) {
            $categoryIds[] = $category['id'];
            if ($category['id'] == $categoryId) $currentCategory = (object) $category;
        }

        $productCategories = ProductCategory::find([
            'categoryId IN ({category_ids:array})',
            'bind' => [
                'category_ids' => $categoryIds
            ]
        ])->toArray();
        $productIds = [0];
        foreach ($productCategories as $productCategory) {
            $productIds[] = $productCategory['id'];
        }

        $products = Product::find([
            'id IN ({product_ids:array})',
            'bind' => [
                'product_ids' => $productIds
            ]
        ]);
        $this->view->setVars(['products' => $products, 'category' => $currentCategory]);
    }

    public function indexAction()
    {

    }

}
